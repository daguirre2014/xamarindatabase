using SQLite;
using People.Droid.Infrastructure;
using Android.App;
using System.IO;
using People.Infrastructure;
using System;

[assembly: Xamarin.Forms.Dependency(typeof(AppRuntimeSettingsDroid))]
namespace People.Droid.Infrastructure
{
    class AppRuntimeSettingsDroid : AppRuntimeSettingsBase
    {
        public override SQLiteAsyncConnection CreateSqLiteConnection()
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            string dbPath = Path.Combine(path, DatabaseFilename);

            CopyDatabaseIfNotExists(dbPath, path);

            var connection = new SQLiteAsyncConnection(dbPath);

            return connection;
        }

        public override void CopyDatabaseIfNotExists(string dbPath, string path)
        {
			
			if (ConstantsDataBase.updateDataBase)
            {
                using (var br = new BinaryReader(Application.Context.Assets.Open(DatabaseFilename)))
                {
                
                    using (var bw = new BinaryWriter(new FileStream(dbPath, FileMode.OpenOrCreate)))
                    {
                        byte[] buffer = new byte[2048];
                        int length = 0;
                        while ((length = br.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            bw.Write(buffer, 0, length);
                        }
                    }
                }
            }
        }
    }
}