﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Preferences;

namespace People.Droid
{
	[Activity(Label = "People", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			ConstantsDataBase.updateDataBase = false;
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
			int dataBaseVersion = prefs.GetInt(ConstantsDataBase.dataBasePreferences, 0);
			if (ConstantsDataBase.dataBaseVersionLocal > dataBaseVersion) 
			{
				ConstantsDataBase.updateDataBase = true;
				ISharedPreferencesEditor editor = prefs.Edit();
				editor.PutInt(ConstantsDataBase.dataBasePreferences,dataBaseVersion + 1);
				editor.Apply();
			}


		      LoadApplication(new People.App());
            

        }


    }
}