﻿using System;
using Foundation;
using UIKit;

namespace People.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			ConstantsDataBase.updateDataBase = false;
			string valueVersion = NSUserDefaults.StandardUserDefaults.IntForKey(ConstantsDataBase.dataBasePreferences).ToString();
			int dataBaseVersion = 0;
			try {
				dataBaseVersion = int.Parse(valueVersion);
			} catch (Exception e)
			{
				dataBaseVersion = 0;
			}
			if (ConstantsDataBase.dataBaseVersionLocal > dataBaseVersion)
			{ 
				ConstantsDataBase.updateDataBase = true;
				NSUserDefaults.StandardUserDefaults.SetInt(dataBaseVersion + 1, ConstantsDataBase.dataBasePreferences);
			}
			LoadApplication(new People.App());

			return base.FinishedLaunching(app, options);
		}

	}
}