﻿using Foundation;
using People.Infrastructure;
using People.iOS.Infrastructure;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(AppRuntimeSettings_iOS))]
namespace People.iOS.Infrastructure
{
    class AppRuntimeSettings_iOS : AppRuntimeSettingsBase
    {

        public override SQLiteAsyncConnection CreateSqLiteConnection()
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = System.IO.Path.Combine(docFolder, "..", "Library", "Databases");

            if (!System.IO.Directory.Exists(libFolder))
            {
                System.IO.Directory.CreateDirectory(libFolder);
            }
            string dbPath = Path.Combine(libFolder, DatabaseFilename);

            CopyDatabaseIfNotExists(dbPath,string.Empty);
            var connection = new SQLiteAsyncConnection(dbPath);
            return connection;
        }
    
        public override void CopyDatabaseIfNotExists(string dbPath, string path)
        {
            if (ConstantsDataBase.updateDataBase)
            {
                var existingDb = NSBundle.MainBundle.PathForResource(DatabaseFilename.Split('.')[0], DatabaseFilename.Split('.')[1]);
				if (File.Exists(dbPath))
				{
					File.Delete(dbPath);
				}
                File.Copy(existingDb, dbPath);
            }
        }
    }
}
