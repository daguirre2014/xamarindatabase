﻿using SQLite;

namespace People.Models
{
   public class people
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Lastname { get; set; }
    }
}