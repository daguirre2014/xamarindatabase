﻿using Autofac;
using People.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace People
{
    class DependencyInjection
    {
        private static IAppRuntimeSettings GetApplicationRuntimeSettings()
        {
            var platformSpecificSettings = Xamarin.Forms.DependencyService.Get<IAppRuntimeSettings>();
            if (platformSpecificSettings == null)
            {
                throw new InvalidOperationException($"Missing '{typeof(IAppRuntimeSettings).FullName}' implementation! Implementation is required.");
            }
            return platformSpecificSettings;
        }

        public static IContainer Initialize()
        {
            var containerBuilder = new ContainerBuilder();
            var applicationRuntimeSettings = GetApplicationRuntimeSettings();

            RegisterPlatformSpecificObjects(containerBuilder, applicationRuntimeSettings);

            var Container = containerBuilder.Build();

            return Container;
        }

        private static void RegisterPlatformSpecificObjects(ContainerBuilder containerBuilder, IAppRuntimeSettings applicationRuntimeSettings)
        {
            containerBuilder.RegisterInstance(applicationRuntimeSettings).AsImplementedInterfaces().SingleInstance();

            var sqlLiteConnection = applicationRuntimeSettings.CreateSqLiteConnection();
            containerBuilder.RegisterInstance(sqlLiteConnection).AsSelf().SingleInstance();
        }
    }
}
