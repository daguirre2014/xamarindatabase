﻿using Autofac;
using People.Infrastructure;
using SQLite;
using System;
using Xamarin.Forms;

namespace People
{
	public class App : Application
	{
	//	public static PersonRepository PersonRepo { get; private set; }

		public App()
		{
             // var sqliteConnection = DependencyInjection.Initialize().Resolve<SQLiteAsyncConnection>();
             this.MainPage = new MainPage();
		}
    }
}