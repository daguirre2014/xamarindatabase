﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace People.Infrastructure
{
    public interface IAppRuntimeSettings
    {
        SQLiteAsyncConnection CreateSqLiteConnection();
        void CopyDatabaseIfNotExists(string dbPath, string path);
        string DatabaseFilename { get; }
    }
}
