﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace People.Infrastructure
{
    public abstract class AppRuntimeSettingsBase : IAppRuntimeSettings
    {
        public abstract SQLiteAsyncConnection CreateSqLiteConnection();

        public abstract void CopyDatabaseIfNotExists(string dbPath, string path);
        public string DatabaseFilename { get; } = "people.db3";
        
    }
}
