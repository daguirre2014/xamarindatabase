﻿using System;
using System.Collections.Generic;
using System.Linq;
using People.Models;
using SQLite;
using System.Threading.Tasks;
using Autofac;

namespace People
{
	public class PersonRepository
	{
		private  readonly SQLiteAsyncConnection conn = DependencyInjection.Initialize().Resolve<SQLiteAsyncConnection>();
    // private SQLite
    public string StatusMessage { get; set; }

		public PersonRepository()
		{
                   
		}
      
		public  async Task<List<people>> GetAllPeople() 
		{
            try
            {
                List<people> result = await conn.QueryAsync<people>("SELECT ID,NAME FROM people");
                return result;
            }
            catch (Exception ex)
            {
                throw new CustomSQLiteException("Error al obtener la data",ex);
            }
            finally {

            }
		}

        public  async Task<people> GetPeopleId(int id)
        {
            try
            {
                List<people> result = await conn.QueryAsync<people>("SELECT ID,NAME,Lastname FROM people where id =?");
                return result.ElementAt(id);
            }
            catch (Exception ex)
            {
                throw new CustomSQLiteException("Error al obtener la data");
            }
            finally {

            }
        }
        public  async Task<int> deletePeopleById(int id)
        {
            try
            {
                int result = await conn.ExecuteAsync("DELETE FROM PEOPLE WHERE ID =?", id);
                return result;
            }
            catch (Exception ex)
            {
                throw new CustomSQLiteException("Error al eliminar el registro", ex);
            }
            finally {

            }
        }

        public  async Task<int> updatePeopleById(people people)
        {
            try
            {
                int result = await conn.ExecuteAsync("UPDATE PEOPLE SET NAME=?, LASTNAME=? WHERE ID =?", people.Name, people.Lastname, people.Id);
                return result;
            }
            catch (Exception ex)
            {
                throw new CustomSQLiteException("Error al actualizar el registro", ex);
            }
            finally
            {

            }
        }

        public  async Task<int> insertPeople(string name)
        {
            try
            {
                
                int result = await conn.ExecuteAsync("INSERT INTO people (Name,LastName) VALUES(?,?)", name, name);
                return result;
            }
            catch (Exception ex)
            {
                throw new CustomSQLiteException("Error al insertar el registro", ex);
            }
            finally
            {

            }
        }
    }
}