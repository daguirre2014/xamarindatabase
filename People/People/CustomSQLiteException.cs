﻿using System;

namespace People
{
    internal class CustomSQLiteException : Exception
    {
        public CustomSQLiteException()
        {

        }

        public CustomSQLiteException(string message) : base(message)
        {
        }

        public CustomSQLiteException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}