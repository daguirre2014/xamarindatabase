﻿using People.Models;
using System;
using System.Collections.ObjectModel;

namespace People
{
    public partial class MainPage
    {
    PersonRepository personRepo;
    public MainPage()
        {
            InitializeComponent();
            personRepo = new PersonRepository();
        }

        public async void OnNewButtonClicked(object sender, EventArgs args)
        {
            statusMessage.Text = "";

            await personRepo.insertPeople(newPerson.Text);
            statusMessage.Text = personRepo.StatusMessage;
        }
         
        public async void OnGetButtonClicked(object sender, EventArgs args)
        {
            statusMessage.Text = "";

            ObservableCollection<people> people = new ObservableCollection<people>(await personRepo.GetAllPeople());
            peopleList.ItemsSource = people;
        }
    }
}
